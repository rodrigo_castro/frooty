﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Frooty.Models;
using Web.Frooty.Util;
namespace Web.Frooty.Controllers
{
    public class PartnerController : Controller
    {
        //
        // GET: /Partner/

        public ActionResult Index()
        {
            frootyacai_siteEntities db = new frootyacai_siteEntities();
            var result = (from a in db.tb_business select a).ToList();

            var Country = (from a in db.tb_country where a.fl_active == "1" select new { a.id_country, a.ds_name }).ToArray();
            ViewData["id_country"] = new SelectList(Country, "id_country", "ds_name");

            return View(result);
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult CadPartners(tb_partners dados)
        {
            try
            {
                frootyacai_siteEntities db = new frootyacai_siteEntities();

                var email = dados.email.ToUpper();

                var Result = (from a in db.tb_partners where a.email.ToUpper() == email select a).FirstOrDefault();

                if (Result == null)
                {
                    dados.password = Security.Encrypt(dados.password);
                    dados.id_statusPartners = 1;
                    dados.ts_user_cadm = DateTime.Now;
                    dados.ts_user_manu = DateTime.Now;

                    db.tb_partners.Add(dados);
                    db.SaveChanges();

                    return Json(new { success = true, id = dados.id_statusPartners }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new { success = false, msg = "Existing Partners" }, JsonRequestBehavior.AllowGet);
                }

               
                
            }
            catch
            {
                return Json(new { success = false,msg = "Try later" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult listState(int id_country)
        {
            try
            {
                frootyacai_siteEntities db = new frootyacai_siteEntities();

                var result = (from a in db.tb_states where a.fl_active == "1" && a.id_country == id_country select new { a.id_state, a.ds_name }).ToArray();

                return Json(new { result }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { result = false }, JsonRequestBehavior.AllowGet);
            }

        }

    }
}
